import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

/**
 * A subclass of JPanel that draws a list of points.
 *
 * ************ Not fully implemented! ********************
 * 
 * @author cusack
 *
 */
public class ColoredPointPanel extends JPanel {
	ArrayList<ColoredPoint>	points	= null;

	public ColoredPointPanel() {
		super();
	}

	public ColoredPointPanel(ArrayList<ColoredPoint> points) {
		super();
		this.points = points;
	}

	public void setPoints(ArrayList<ColoredPoint> points) {
		this.points = points;
		repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (points != null) {
			for (ColoredPoint point : points) {
				Color color = point.getColor();
				g.setColor(color);
				
				int x = point.getX();
				int y = point.getY();
				
				g.fillOval(x, y, 7, 7);
			}
		}
	}
}
